const gulp = require('gulp');

const clean = require('gulp-clean');
const cleanCss = require("gulp-clean-css");
const htmlmin = require("gulp-htmlmin");
const rjs = require('gulp-requirejs');
const ngAnnotate = require('gulp-ng-annotate');
const uglifyes = require('uglify-es');
const uglifyComposer = require('gulp-uglify/composer');
const uglify = uglifyComposer(uglifyes, console);

const browserSync = require('browser-sync').create();
const httpProxy = require('http-proxy-middleware').createProxyMiddleware;

function watch() {
    browserSync.init({
        server: {
            baseDir: '.',
            //directory: true,
            index: 'app/index.html',
            middleware: [httpProxy('/note/**', {
                target: 'http://localhost:8080',
                changeOrigin: true
            })]
        },
        port: 8081,
        files: 'app/**/*',
        watchEvents: ['add', 'change', 'unlink']
    });
}

const build = gulp.series(function cleanTempAndDistDirs() {
    // clean temp and dist
    return gulp.src(['temp', 'dist'], {
            read: false, allowEmpty: true
        })
        .pipe(clean({ force: true }));
}, gulp.parallel(function buildCSS() {
    // cssmin
    return gulp.src(['app/**/*.css', '!app/lib/**/*.css'])
        .pipe(cleanCss({
            rebase: false,
            compatibility: '*',
            keepSpecialComments: '*'
        }))
        .pipe(gulp.dest('temp/dist'));
}, function buildHTML() {
    // htmlmin
    return gulp.src('app/**/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true,
            conservativeCollapse: false,
            collapseInlineTagWhitespace: false,
            //preserveLineBreaks: true,
            keepClosingSlash: true,
            ignoreCustomFragments: [],
            minifyCSS: {
                compatibility: '*',
                keepSpecialComments: '*'
            },
            minifyJS: true
        }))
        .pipe(gulp.dest('temp/dist'));
}, function buildJS() {
    // jsmin
    return gulp.src(['app/**/*.js', '!app/lib/**/*.js'])
        .pipe(ngAnnotate())// angular.js
        .pipe(uglify())// jsmin
        .pipe(gulp.dest('temp/dist'));
}, function copyImages() {
    // images
    return gulp.src('app/images/**/*').pipe(gulp.dest('temp/dist/images'));
}, function copyLibModules() {
    // lib
    return gulp.src('app/lib/**/*').pipe(gulp.dest('temp/dist/lib'));
}), function buildRequirejs() {
    // build require.js
    return new Promise(function(resolve, reject) {
        try {
            resolve(rjs({
                appDir: 'temp/dist/',
                baseUrl: './',
                siteRoot: '../temp/',
                //buildCSS: false,
                mainConfigFile: 'temp/dist/main.js',
                useStrict: false,
                //findNestedDependencies: true,
                removeCombined: true,
                optimize: 'none',//'uglify2',
                skipDirOptimize: true,
                optimizeCss: 'none',//'standard.keepLines',
                name: 'build',
                //include: 'app',
                //insertRequire: ['build'],
                dir: 'temp/app/'
            }));
        } catch(e) {
            reject(e);
        }
    });
}, function copyResources() {
    // resources
    return gulp.src('temp/app/**/*').pipe(gulp.dest('dist'));
}, function cleanTempDir() {
    // clean temp
    return gulp.src('temp', { read: false }).pipe(clean({ force: true }));
});

exports.default = watch;
exports.build = build;
