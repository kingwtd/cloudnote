# Cloudnote 云笔记

## 项目简介

Cloudnote云笔记 —— 基于ACE、Summernote、Handsontable和ZRender的在线云笔记，支持文本（富文本）、表格、绘图以及其他文件的预览（图片、PDF、音视频和Office文档等）几大功能。操作界面和风格与本地的编辑器相类似，基本上与用户日常办公软件的操作习惯一致。各类文件的操作都保存了历史版本，更好地为用户提供了文件的版本管理。

在线示例地址：http://47.92.196.8:8080/

## 安装运行

1. 安装[Node.js](https://nodejs.org/zh-cn/)

2. 安装[Unoconv](http://dag.wiee.rs/home-made/unoconv/)

3. 在控制台执行以下命令：

  &gt; npm install

  &gt; npm start

> 若运行压缩版，请将config.js中的debug置为false，然后执行`npm run build`，再执行`npm start`。

## 使用说明

整个操作界面分为菜单栏、导航（工具）栏、目录栏、工作区域、状态栏。目录栏与工作区域可拖动分隔线来改变大小。**用户需要先新建模块后，在模块的基础上新建文件夹和文件。**_由于javascript访问本地剪贴板受限，粘贴按钮暂不可使用，可以通过快捷键【Ctrl+V】来粘贴。_ 操作界面如图：

![Cloudnote](app/images/cloudnote.png)

### 1. 菜单栏

- 文件管理：文件的增删改查和版本历史

- 文档编辑：文档的各项编辑操作功能

- 视图设置：设置操作界面的显示区域

- 工具选项：ACE编辑器的基础操作配置

- 用户帮助：快捷键提示和用户管理

### 2. 导航（工具）栏

- 提供用户对操作界面、文件管理以及文件编辑的快捷操作。

### 3. 目录栏

- 展示文件目录，可折叠、展开和隐藏，**双击可打开文件**。

- **点击右键会弹出菜单**，然后可以点击菜单进行相应操作。

### 4. 工作区域

- 文件面板会列出当前已打开的文件，**面板上的文件列表可进行移动，双击可切换工作区域全屏展示，右键可点击关闭等操作**。

- 文件面板右侧有下拉文件列表，**可过滤查找或选择打开其中一个文件**。

- 编辑区域可选择工作栏上的工具进行编辑，**也可右键菜单选择相应的操作，还可以通过快捷键来操作**。

### 5. 状态栏

- 展示当前编辑文件的各项状态

## 界面截图

### Markdown（文本）

![Markdown](app/images/markdown.png)

### Summernote（文档）

![Summernote](app/images/summernote.png)

### Handsontable（表格）

![Handsontable](app/images/handsontable.png)

### ZRender（图形）

![ZRender](app/images/zrender.png)
