define(['app'], function(app) {
    const renderers = [{
        types: 'sdoc',
        template: '<summernote file="file" context="context"/>'
    }, {
        types: 'hxls',
        template: '<handsontable file="file" context="context"/>'
    }, {
        types: 'zsvg',
        template: '<zrender file="file" context="context"/>'
    }, {
        types: 'md',
        template: '<markdown file="file" context="context"/>'
    }, {
        types: 'html,css,js,c,cpp,java,cs,py,go,swift,groovy,sql,sh,log,xml,yml,json,txt',
        template: '<aceeditor file="file" context="context"/>'
    }, {
        types: 'gif,png,jpg',
        template: '<imageviewer file="file" context="context"/>'
    }, {
        types: 'avi,wav,wmv,swf,flv,mp3,mpg,mp4,rm,pdf,xaml',
        template: '<mediaviewer file="file" context="context"/>'
    }, {
        types: 'docx',
        template: '<officeviewer file="file" context="context"/>'
    }, {
        types: '*',
        template: '<blankpage file="file" context="context"/>'
    }];

    app.directive('renderer', function() {
        return {
            restrict: 'E',
            replace: true,
            template: '<div ng-switch="file.extension" class="edit-zone"></div>',
            scope: {
                file: '=',
                context: '='
            },
            compile: function(element) {
                _.forEach(renderers, function(renderer) {
                    if (renderer.types == '*') {
                        $(renderer.template).attr('ng-switch-default', '').appendTo(element);
                    } else {
                        var types = renderer.types.split(',');
                        _.forEach(types, function(type) {
                            $(renderer.template).attr('ng-switch-when', type).appendTo(element);
                        });
                    }
                });
            }
        };
    });
});
