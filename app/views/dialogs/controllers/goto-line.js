define(['app'], function(app) {
    app.controller('gotoLine', function($uibModalInstance, _context) {
        var self = this;
        self.line = null;
        self.maxLine = 1;

        self.init = function() {
            self.maxLine = _context.maxLine;
        }

        self.goto = function(valid) {
            if (valid) {
                $uibModalInstance.close(self.line);
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
