define(['app'], function(app) {
    app.controller('editLink', function($uibModalInstance, _context) {
        var self = this;
        self.text = '';
        self.url = 'http://';

        self.init = function() {
            self.text = _context.text || '';
            self.url = _context.url || 'http://';
        }

        self.submit = function (valid) {
            if (valid) {
                $uibModalInstance.close({ text: self.text, url: self.url });
            }
        }

        self.cancel = function () {
            $uibModalInstance.dismiss(false);
        }
    });
});
