define(['app'], function(app) {
    app.controller('searchbox', function($scope, $timeout, $uibModalInstance, toastr, _context) {
        var self = this;

        self.disabled = false;
        self.context = {
            needle: '',
            replace: '',
            backwards: false,
            wrap: true,
            caseSensitive: false,
            wholeWord: false,
            range: null,
            regExp: false,
            skipCurrent: false
        };

        var editor = null;

        self.init = function() {
            editor = _context.editor;
            self.context.needle = editor.getSession().getTextRange(editor.getSelectionRange());
            $uibModalInstance.rendered.then(function() {
                var jqDialog = $('#searchbox').parents('.modal-dialog');
                jqDialog.draggable({
                    cursor: 'move',
                    handle: '.modal-header',
                    delay: 200
                });
            });

            $scope.$watch('dvm.context.needle', function (newValue) {
                self.context.skipCurrent = false;
                editor.find(self.context.needle, self.context);
            });

            $scope.$on('editor.active', function(event, current) {
                editor = current;
                $timeout(function() {
                    self.disabled = false;
                    //self.context.skipCurrent = false;
                    //editor.find(self.context.needle, self.context);
                });
            });

            $scope.$on('editor.disable', function(event) {
                $timeout(function() {
                    self.disabled = true;
                });
            });

            $scope.$on('editor.search', function(event, search) {
                $timeout(function() {
                    self.context.needle = search;
                    $('#ace-searchbox [name="needle"]').select();
                });
            });
        }

        self.execute = function(valid) {
            if (valid) {
                self.find();
            }
        }

        self.find = function() {
            if (editor.find(self.context.needle, self.context)) {
                self.context.skipCurrent = true;
            } else {
                toastr.warning('查找不到【' + self.context.needle + '】！');
            }
        }

        self.replace = function() {
            //self.context.skipCurrent = false;
            if (editor.replace(self.context.replace)) {
                self.context.skipCurrent = true;
                editor.find(self.context.needle, self.context);
            } else {
                toastr.warning('查找不到【' + self.context.needle + '】！');
            }
        }

        self.replaceAll = function() {
            if (!editor.replaceAll(self.context.replace)) {
                toastr.warning('查找不到【' + self.context.needle + '】！');
            }
        }

        self.close = function() {
            $uibModalInstance.close();
        }
    });
});
