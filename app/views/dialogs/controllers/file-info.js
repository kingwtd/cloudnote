define(['app', 'ace-language-tools'], function(app) {
    app.controller('fileInfo', function($http, $uibModalInstance, _context) {
        var self = this;

        var infoViewer = null;

        self.init = function() {
            self.sha = _context.sha;

            infoViewer = ace.edit('info-viewer', {
                mode: 'ace/mode/text',
                theme: 'ace/theme/chrome',
                fontSize: 14,
                readOnly: true,
                useWorker: false,
                showPrintMargin: false,
                showFoldWidgets: false,
                displayIndentGuides: false,
                highlightGutterLine: false,
                highlightActiveLine: false,
                autoScrollEditorIntoView: true
            });

            $http({
                method: 'POST',
                url: '/note/git/file',
                data: { file: _context.path, sha: self.sha },
                transformResponse: function(response) {
                    return response;
                }
            }).then(function(response) {
                if (response.status == 200) {
                    infoViewer.setValue(response.data, -1);
                    infoViewer.resize();
                }
            });
        }

        self.resize = function() {
            infoViewer.resize();
        }

        self.close = function() {
            $uibModalInstance.close();
        }
    });
});
