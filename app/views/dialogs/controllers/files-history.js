define(['app'], function(app) {
    app.filter('fileKeyword', function($sce) {
        return function(text, keyword) {
            if (text && keyword) {
                text = encodeURIComponent(text);
                keyword = encodeURIComponent(keyword);
                // replace
                var highlight = text.replace(new RegExp(keyword, 'gi'),
                        '<span style="color:whitesmoke;background-color:darkgrey;">$&</span>');
                highlight = decodeURIComponent(highlight);
                return $sce.trustAsHtml(highlight);
            }
            return $sce.trustAsHtml(text);
        }
    });

    app.filter('fileStatus', function($sce) {
        return function(status) {
            var oStatus = {};
            switch(status) {
                case 1:
                    oStatus = { color: 'green', text: 'A', title: 'ADDED' };
                    break;
                case 2:
                    oStatus = { color: 'red', text: 'D', title: 'DELETED' };
                    break;
                case 3:
                    oStatus = { color: 'blue', text: 'M', title: 'MODIFIED' };
                    break;
                default:
                    oStatus = { color: 'black', text: status, title: status };
                    break;
            }
            var html = '<font color="' + oStatus.color + '" title="' + oStatus.title + '">[<span'
                     + '  style="display:inline-block;width:1em;text-align:center">' + oStatus.text + '</span>]'
                     + '</font>';
            return $sce.trustAsHtml(html);
        }
    });

    app.controller('filesHistory', function($http, $uibModalInstance, _context, _dialog) {
        var self = this;
        self.query = '';
        self.commits = [];
        self.ending = true;

        var lastSha = null;

        self.init = function() {
            loadHistory();
        }

        function loadHistory() {
            $http.post('/note/git/logs', {
                parent: _context.path,
                file: self.query,
                step: 20,
                sha: lastSha
            }).then(function(response) {
                if (response.status == 200) {
                    self.ending = response.data.ending;
                    _.forEach(response.data.commits, function(commit) {
                        lastSha = commit.sha;
                        self.commits.push(commit);
                    });
                }
            });
        }

        self.search = function() {
            self.commits = [];
            self.ending = true;
            lastSha = null;
            loadHistory();
        }

        self.load = function() {
            loadHistory();
        }

        self.show = function(commit, file) {
            var filename = file.path;
            if (filename.lastIndexOf('/') > 0) {
                filename = filename.substring(filename.lastIndexOf('/') + 1);
            }
            _dialog.open().showFileHistory({
                file: filename,
                path: file.path
            }).then(function(refreshable) {
                if (refreshable) {
                    self.commits = [];
                    lastSha = null;
                    loadHistory();
                }
            });
        }

        self.resize = function(height, width, fullscreen) {
            if (fullscreen) {
                $('.history-wrapper').css('max-height', (height - 81) + 'px');
            } else {
                $('.history-wrapper').css('max-height', '700px');
            }
        }

        self.close = function() {
            $uibModalInstance.close();
        }
    });
});
