define(function() {
    return {
        baseUrl: '/app/',
        defaultPath: '/:path*?',
        disableContextmenu: true,
        textTypes: [
            'html,css,js',
            'c,cpp,java,cs',
            'py,go,swift,groovy',
            'sql,sh,log',
            'xml,yml,json,txt',
            'md,sdoc,hxls,zsvg'
        ].join(',').split(',')
    };
});
