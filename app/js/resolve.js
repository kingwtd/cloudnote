define(['config'], function(config) {
    return function(options) {
        if (options.templateUrl) {
            options.templateUrl = config.baseUrl + options.templateUrl;
        }

        if (options.controllerUrl) {
            var controllerUrl = config.baseUrl + options.controllerUrl;
            delete options.controllerUrl;
            options.resolve = options.resolve || {};
            options.resolve['resolve'] = ['$q', '$rootScope', function ($q, $rootScope) {
                var defer = $q.defer();
                require([controllerUrl], function () {
                    $rootScope.$apply(function() {
                        defer.resolve();
                    });
                });
                return defer.promise;
            }];
        }

        return options;
    };
});
