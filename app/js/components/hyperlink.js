define([
    'js/resolve',
    'js/components/dialog'
], function(resolve) {
    angular.module('app.hyperlink', ['app.dialog'])
        .directive('hyperlink', hyperLinkDirective);

    function hyperLinkDirective(_dialog) {
        return resolve({
            restrict: 'E',
            replace: true,
            template: '<button class="btn btn-default" title="超链接" ng-click="select()"><i class="fa fa-link"></i></button>',
            scope: {
                onSelected: '<'
            },
            link: function(scope) {
                scope.select = function() {
                    _dialog.open().editLink({}).then(function(link) {
                        if (typeof(scope.onSelected) === 'function') {
                            scope.onSelected(link);
                        }
                    });
                };
            }
        });
    }
});
