define(['jquery-media'], function() {
    angular.module('app.mediaviewer', [])
        .directive('mediaviewer', mediaViewerDirective);

    function mediaViewerDirective($timeout) {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="media-viewer">' +
                      '  <div class="media-content"></div>' +
                      '</div>',
            scope: {
                file: '=',
                context: '='
            },
            link: function(scope, element) {
                // load media
                $timeout(function() {
                    var params = {
                        'type': '.' + scope.file.extension,
                        'file': encodeURIComponent(scope.file.path)
                    };
                    if (scope.file.path.lastIndexOf(scope.file.extension) < 0) {
                        params['format'] = scope.file.extension;
                    }
                    var src = '/note/file/preview?' + _.map(params, function(value, name) {
                        return name + '=' + value;
                    }).join('&');
                    $('.media-content', element).media({
                        src: src,
                        autoplay: false,
                        bgColor: 'black',
                        width: element.width(),
                        height: element.height()
                    }, function() {
                        // TODO
                    }, function() {
                        if ($('.media-content>*', element).is('iframe')) {
                            $('.media-content', element).css('height', '100%');
                            // show load processing
                            var completed = false;
                            setTimeout(function() {
                                if (!completed) {
                                    $('<div class="media-loading"></div>')
                                        .append('<div class="loading-info">正在加载文件，请稍候...</div>')
                                        .appendTo(element);
                                }
                            }, 250);
                            // hide load processing after loaded
                            $('.media-content>iframe', element).on('load', function() {
                                completed = true;
                                $timeout(function() {
                                    $('.media-loading', element).remove();
                                }, 300);
                            });
                        }
                    });
                });

                scope.file.renderer = function() {
                    // navbar
                    scope.context.navbar['file']['save'].enabled = false;
                    scope.context.navbar['edit']['undo'].enabled = false;
                    scope.context.navbar['edit']['redo'].enabled = false;
                    scope.context.navbar['edit']['cut'].enabled = false;
                    scope.context.navbar['edit']['copy'].enabled = false;
                    scope.context.navbar['edit']['paste'].enabled = false;
                    // statusbar
                    scope.context.statusbar.link = scope.file.path;
                    scope.context.statusbar.statuses = ['只读'];
                }

                // destroy
                scope.$on('$destroy', function() {
                    element.remove();
                });
            }
        };
    }
});
