define([
    'js/resolve',
    'js/resources',
    'angular-sanitize',
    'angular-ui-bootstrap'
], function(resolve, resources) {
    angular.module('app.dialog', ['ngSanitize', 'ui.bootstrap'])
        .factory('_dialog', dialogFactory)
        .run(function($templateCache) {
            $templateCache.put('templates/dialog/alert.html',
                '<div class="modal-header">' +
                '  <button type="button" class="close" ng-click="ok()">&times;</button>' +
                '  <h4 class="modal-title">{{title}}</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                '  <p ng-bind-html="message"></p>' +
                '</div>' +
                '<div class="modal-footer">' +
                '  <button type="button" class="btn btn-default" ng-click="ok()" autofocus>' +
                '    <i class="fa fa-check"></i> 确定</button>' +
                '</div>');
            $templateCache.put('templates/dialog/confirm.html',
                '<div class="modal-header">' +
                '  <button type="button" class="close" ng-click="cancel()">&times;</button>' +
                '  <h4 class="modal-title">{{title}}</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                '  <p ng-bind-html="message"></p>' +
                '</div>' +
                '<div class="modal-footer">' +
                '  <button type="button" class="btn btn-default" ng-click="ok()" autofocus>' +
                '    <i class="fa fa-check"></i> {{sno?\'&nbsp;是&nbsp;\':\'确定\'}}</button>' +
                '  <button type="button" class="btn btn-default" ng-click="no()" ng-if="sno">' +
                '    <i class="fa fa-circle-o"></i> &nbsp;否&nbsp;</button>' +
                '  <button type="button" class="btn btn-default" ng-click="cancel()">' +
                '    <i class="fa fa-times"></i> 取消</button>' +
                '</div>');
        });

    function dialogFactory($q, $uibModal) {
        var _dialog = {};

        _dialog.modal = function(options, _resolve, _reject) {
            var modalInstance = $uibModal.open(angular.extend({
                backdrop: 'static',
                size: 'md',
                keyboard: false
            }, options));

            var defer = $q.defer();
            modalInstance.result.then(function(data) {
                if (angular.isDefined(_resolve)) {
                    _resolve(defer, data);
                } else {
                    defer.resolve(data);
                }
            }, function(data) {
                if (angular.isDefined(_reject)) {
                    _reject(defer, data);
                } else {
                    defer.reject(data);
                }
            });

            return defer.promise;
        }

        _dialog.alert = function(title, message) {
            return _dialog.modal({
                templateUrl: 'templates/dialog/alert.html',
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.title = title;
                    $scope.message = message;
                    $scope.ok = function () {
                        $uibModalInstance.close(true);
                    };
                }]
            }, function() {});
        }

        _dialog.confirm = function(title, message, no) {
            return _dialog.modal({
                templateUrl: 'templates/dialog/confirm.html',
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.title = title;
                    $scope.message = message;
                    $scope.sno = no;
                    $scope.ok = function () {
                        $uibModalInstance.close(true);
                    };
                    $scope.no = function () {
                        $uibModalInstance.close(false);
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss(false);
                    };
                }]
            }, function(defer, data) {
                no ? defer.resolve(data) : defer.resolve(true);
            }, function(defer, data) {
                no ? defer.reject(data) : defer.resolve(false);
            });
        }

        var modalInstances = {};
        _.forEach(resources.dialogs, function(dialog) {
            modalInstances[dialog.method] = function(context) {
                return _dialog.modal(resolve(angular.extend({
                    controllerAs: 'dvm',
                    resolve: {
                        _context: function () {
                            return context;
                        }
                    }
                }, dialog)));
            };
        });
        _dialog.open = function() {
            return modalInstances;
        }

        return _dialog;
    }
});
