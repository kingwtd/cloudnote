define(['js/components/util'], function() {
    angular.module('app.blankpage', ['app.util'])
        .directive('blankpage', blankPageDirective);

    function blankPageDirective(_util) {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="blank-page">' +
                      '  <strong>文件格式不支持，请<a href="javascript:void(0)"' +
                      '      ng-click="download()">下载</a>后用其他工具打开！</strong>' +
                      '</div>',
            scope: {
                file: '=',
                context: '='
            },
            link: function(scope, element) {
                // download
                scope.download = function() {
                    _util.download('/note/file/download', { 'file': scope.file.path });
                }
                // render page
                scope.file.renderer = function() {
                    // navbar
                    scope.context.navbar['file']['save'].enabled = false;
                    scope.context.navbar['edit']['undo'].enabled = false;
                    scope.context.navbar['edit']['redo'].enabled = false;
                    scope.context.navbar['edit']['cut'].enabled = false;
                    scope.context.navbar['edit']['copy'].enabled = false;
                    scope.context.navbar['edit']['paste'].enabled = false;
                    // statusbar
                    scope.context.statusbar.link = scope.file.path;
                    scope.context.statusbar.statuses = [];
                }
            }
        }
    }
});
