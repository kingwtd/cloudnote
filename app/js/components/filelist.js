define([
    'js/resolve',
    'js/components/keyword'
], function(resolve) {
    angular.module('app.filelist', ['app.keyword'])
        .directive('filelist', fileListDirective);

    function fileListDirective() {
        return resolve({
            restrict: 'E',
            replace: true,
            templateUrl: 'js/templates/filelist.html',
            scope: {
                files: '=',
                onSelected: '<'
            },
            link: function(scope, element) {
                if (!scope.files) {
                    scope.files = [];
                }
                scope.select = function(file) {
                    if (typeof(scope.onSelected) === 'function') {
                        scope.onSelected(file);
                    }
                }
                element.parent().on('show.bs.dropdown', function() {
                    scope.$apply(function() {
                        scope.filter = '';
                    });
                }).on('shown.bs.dropdown', function() {
                    element.find('input').focus();
                });
            }
        });
    }
});
