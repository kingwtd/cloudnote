define([
    'js/resolve',
    'angular-ui-bootstrap',
    'js/components/dialog'
], function(resolve) {
    angular.module('app.sourcecode', ['ui.bootstrap', 'app.dialog'])
        .directive('sourcecode', sourceCodeDirective);

    function sourceCodeDirective(_dialog) {
        return resolve({
            restrict: 'E',
            replace: true,
            templateUrl: 'js/templates/sourcecode.html',
            scope: {
                position: '=',
                onSelected: '<'
            },
            link: function(scope) {
                scope.select = function(action) {
                    var lang = '', code = '';
                    switch(action) {
                        case 'hr':
                            code = code || '<hr>';
                        case 'blockquote':
                            code = code || '<blockquote></blockquote>';
                        case 'code':
                            code = code || '<code></code>';
                            if (typeof(scope.onSelected) === 'function') {
                                scope.onSelected(action, code);
                            }
                            break;
                        case 'html':
                            lang = 'html';
                        case 'blockcode':
                            _dialog.open().editCode({
                                lang: lang, checked: lang == 'html' ? true : false
                            }).then(function(result) {
                                if (typeof(scope.onSelected) === 'function') {
                                    scope.onSelected(action, result.code, result.lang);
                                }
                            });
                            break;
                        default: break;
                    }
                }
            }
        });
    }
});
