define([
    'js/resolve',
    'angular-ui-bootstrap'
], function(resolve) {
    angular.module('app.headerlist', ['ui.bootstrap'])
        .directive('headerlist', headerListDirective);

    function headerListDirective() {
        return resolve({
            restrict: 'E',
            replace: true,
            templateUrl: 'js/templates/headerlist.html',
            scope: {
                position: '=',
                onSelected: '<'
            },
            link: function(scope) {
                scope.select = function(level) {
                    if (typeof(scope.onSelected) === 'function') {
                        scope.onSelected(level);
                    }
                }
            }
        });
    }
});
