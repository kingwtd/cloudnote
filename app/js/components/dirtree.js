define(['zTree-exhide'], function() {
    angular.module('app.dirtree', [])
        .directive('dirtree', dirTreeDirective);

    function dirTreeDirective($timeout, $http) {
        var treeIdOffset = 0;
        function getNodePath(node) {
            return _.map(node.getPath(), function(_node) {
                return _node.name;
            }).join('/');
        }
        //
        function excludeNodePath(nodes, parent, exclude) {
            var newNodes = [];
            _.each(nodes, function(node) {
                var path = parent + '/' + node.name;
                // exclude node
                if (path != exclude) {
                    newNodes.push(node);
                    // exclude node children
                    if (!_.isEmpty(node.children)) {
                        node.children = excludeNodePath(node.children, path, exclude);
                    }
                }
            });
            return newNodes;
        }
        //
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="dirtree"><ul class="ztree"></ul></div>',
            scope: {
                exclude: '<',
                onSelected: '<'
            },
            link: function(scope, element) {
                $http.get('/note/data/tree?dironly=true').then(function(response) {
                    if (response.status == 200) {
                        var treeNodes = response.data || [];
                        if (scope.exclude) {
                            treeNodes = excludeNodePath(treeNodes, '', scope.exclude);
                        }
                        _.each(treeNodes, function(node) {
                            if (node.module) {
                                node.iconSkin = 'module';
                            }
                        });

                        // init tree nodes
                        var zTree = element.find('.ztree');
                        zTree.attr('id', 'dirtree-' +  (treeIdOffset++));
                        var zTreeObj = $.fn.zTree.init(zTree, {
                            view: {
                                //showTitle: false,
                                addHoverDom: function(treeId, treeNode) {
                                    if (!treeNode.editNameFlag && $('#addBtn_' + treeNode.tId).length == 0) {
                                        $('<span class="button add" id="addBtn_' + treeNode.tId + '" title="新建文件夹">')
                                            .bind('click', function() {
                                                var newNode = zTreeObj.addNodes(treeNode, {
                                                    name: '新建文件夹',
                                                    isParent: true,
                                                    isNew: true
                                                })[0];
                                                zTreeObj.editName(newNode);
                                                return false;
                                            })
                                            .bind('focus', function() {
                                                this.blur();
                                            })
                                            .insertAfter($('#' + treeNode.tId + '_span'));
                                    }
                                },
                                removeHoverDom: function(treeId, treeNode) {
                                    $('#addBtn_' + treeNode.tId).unbind().remove();
                                }
                            },
                            edit: {
                                enable: true,
                                removeTitle: '删除文件夹',
                                showRemoveBtn: function(treeId, treeNode) {
                                    return treeNode.isNew;
                                },
                                showRenameBtn: false
                            },
                            callback: {
                                onClick: function(event, treeId, treeNode) {
                                    if (typeof(scope.onSelected) === 'function') {
                                        $timeout(function() {
                                            scope.onSelected(getNodePath(treeNode), treeNode);
                                        });
                                    }
                                },
                                onRemove: function(event, treeId, treeNode) {
                                    var parentNode = treeNode.getParentNode();
                                    if (parentNode.isParent == false) {
                                        //parentNode.open = true;
                                        parentNode.isParent = true;
                                        zTreeObj.updateNode(parentNode);
                                    }
                                }
                            }
                        }, treeNodes);
                    }
                });
            }
        };
    }
});
