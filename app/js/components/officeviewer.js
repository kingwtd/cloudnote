define(['officeToHtml'], function() {
    angular.module('app.officeviewer', [])
        .directive('officeviewer', officeViewerDirective);

    function officeViewerDirective($timeout) {
        var viewerId = 0;
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="office-viewer">' +
                      '  <div class="office-content" id="ovid-{{viewerId}}"></div>' +
                      '</div>',
            scope: {
                file: '=',
                context: '='
            },
            link: function(scope, element) {
                scope.viewerId = viewerId++;
                // load office
                $timeout(function() {
                    $('#ovid-' + scope.viewerId).officeToHtml({
                        url: '/note/file/preview?file=' + encodeURIComponent(scope.file.path),
                        docxSetting: {
                            styleMap: null,
                            includeEmbeddedStyleMap: true,
                            includeDefaultStyleMap: true,
                            convertImage: null,
                            ignoreEmptyParagraphs: false,
                            idPrefix: 'officeview',
                            isRtl: 'auto'
                        }
                    });
                });

                scope.file.renderer = function() {
                    // navbar
                    scope.context.navbar['file']['save'].enabled = false;
                    scope.context.navbar['edit']['undo'].enabled = false;
                    scope.context.navbar['edit']['redo'].enabled = false;
                    scope.context.navbar['edit']['cut'].enabled = false;
                    scope.context.navbar['edit']['copy'].enabled = false;
                    scope.context.navbar['edit']['paste'].enabled = false;
                    // statusbar
                    scope.context.statusbar.link = scope.file.path;
                    scope.context.statusbar.statuses = ['只读'];
                }

                // destroy
                scope.$on('$destroy', function() {
                    element.remove();
                });
            }
        };
    }
});
