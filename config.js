/**
 * Configurations
 * @author Wangtd
 */
const paths = require('./lib/util.js').paths;

module.exports = {
    debug: true,
    port: 8080,
    workDir: paths('./work'),
    noteDir: paths('./work/notes'),
    imageDir: paths('./work/images'),
    security: {
        admins: [],
        mailDomain: '@',
        allowedHosts: true//['127.0.0.1']// false
    },
    sessionConfig: {
        secret: 'cloudnote',
        cookie: { maxAge: 120 * 60 * 1000 },// 120 minutes
        resave: false,
        saveUninitialized: true
    },
    mailConfig: {
        host: 'smtp.qq.com',
        secureConnection: true,
        auth: {
            user: 'example@qq.com',
            pass: 'secret'
        }
    },
    logConfig: {
        appenders: {
            'console': { type: 'console' },
            'logfile': {
                type: 'file',
                filename: './logs/stdout.log',
                maxLogSize: 10485760,
                backups: 10,
                encoding: 'utf-8'
            }
        },
        categories: {
            'default': {
                appenders: ['console', 'logfile'],
                level: 'info'
            }
        }
    }
};
